//
//  ServiceProvider.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class ServiceProvider {
    // MARK: - Shared instance
    static let shared: ServiceProvider = ServiceProvider()

    private(set) lazy var locationService: LocationService = {
        return GeneralLocationService()
    }()

    private(set) lazy var urlSessionController = URLSessionController()

    private(set) lazy var imageCacheService: ImageCacheService = {
        return GeneralImageCacheService()
    }()

    private(set) lazy var coreDataService: CoreDataProtocol = {
        return CoreDataService.sharedInstance
    }()

    private(set) lazy var coreDataMapperService: CoreDataMapperService = {
        return GeneralCoreDataMapperService(coreDataService: coreDataService)
    }()

    private(set) lazy var keyValueStorage: KeyValueStorage = {
        return GeneralKeyValueStorage(userDefaults: UserDefaults.standard)
    }()
}

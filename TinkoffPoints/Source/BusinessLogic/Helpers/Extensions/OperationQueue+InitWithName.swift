//
//  OperationQueue+InitWithName.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

extension OperationQueue {
    convenience init(name: String) {
        self.init()
        self.name = name
    }
}

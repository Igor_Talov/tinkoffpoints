//
//  HTTPError.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

enum HTTPError: Error {
    case clientError
    case serverError(Data)
}

//
//  HTTPMethod.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case head = "HEAD"
    case delete = "DELETE"
}

//
//  DataCompletion.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

typealias DataClosure = () -> (Data?)
typealias DataCompletion = ((Data?, URLResponse?, Error?) -> Void)

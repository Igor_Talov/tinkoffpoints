//
//  URLSessionController.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

protocol URLSessionControllerDelegate: class {
    var serviceURL: URL { get }
}

class URLSessionController: NSObject {
    // MARK: - Public properties
    weak var delegate: URLSessionControllerDelegate?
    lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration,
                                 delegate: self,
                                 delegateQueue: nil)
        session.configuration.httpAdditionalHeaders = nil
        return session
    }()

    // MARK: - Initialization
    override init() { }
}

extension URLSessionController: EndpointRequest {
    func perform(endpoint: APIEndpoint,
                 completion: @escaping DataCompletion) {
        guard let url = URL(string: endpoint.baseURLString) else {
            return
        }

        var request = URLRequest(url: url,
                                 cachePolicy: .reloadIgnoringLocalCacheData,
                                 timeoutInterval: endpoint.timeoutInterval)
        request.httpMethod = endpoint.method.rawValue

        if let headers = endpoint.headers {
            for (header, value) in headers {
                request.addValue(value, forHTTPHeaderField: header)
            }
        }

        let task = session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(nil, response, error)
                return
            }

            completion(data, response, nil)
        }
        task.resume()
    }
}

extension URLSessionController: URLSessionDelegate { }

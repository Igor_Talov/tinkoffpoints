//
//  TinkoffEndpoint.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import Foundation

enum TinkoffEndpoint {
    case getPoint(coordinates: CLLocationCoordinate2D, radius: Int)
    case getPartners(accountTtype: String)
    case getImage(name: String, dpi: String)
    case getLastModified(name: String, dpi: String)
}

extension TinkoffEndpoint: APIEndpoint {
    var environmentBaseURLString: String {
        switch self {
        case .getPartners(accountTtype:),
             .getPoint(coordinates: radius:):
            return "https://api.tinkoff.ru"
        case .getImage(name: dpi:):
            return "https://static.tinkoff.ru"
        case .getLastModified(name: dpi:):
            return "https://static.tinkoff.ru"
        }

    }

    var baseURLString: String {
        environmentBaseURLString + path
    }

    var path: String {
        switch self {
        case let .getPoint(coordinates: coordinates,
                           radius: radius):
            return "/v1/deposition_points?latitude=\(coordinates.latitude)&longitude=\(coordinates.longitude)&radius=\(radius)"
        case let .getPartners(accountTtype):
            return "/v1/deposition_partners?accountType=\(accountTtype)"
        case let .getImage(name, dpi):
            return "/icons/deposition-partners-v3/\(dpi)/\(name.lowercased()).png"
        case let .getLastModified(name, dpi):
            return "/icons/deposition-partners-v3/\(dpi)/\(name.lowercased()).png"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .getPartners(accountTtype:),
             .getPoint(coordinates: radius:),
             .getImage(name: dpi:):
            return .get
        case .getLastModified(name: dpi:):
            return .head
        }
    }

    var headers: [String: String]? {
        return nil
    }

    var timeoutInterval: TimeInterval {
        return 30
    }
}

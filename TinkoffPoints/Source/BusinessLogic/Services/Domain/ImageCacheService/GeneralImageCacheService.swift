//
//  GeneralImageCacheService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralImageCacheService {
    // MARK: - Private properties
    private let fileManager = FileManager.default
}

extension GeneralImageCacheService: ImageCacheService {
    func save(imageData: Data, imageName: String) {
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if !fileManager.fileExists(atPath: imagePath) {
            fileManager.createFile(atPath: imagePath as String, contents: imageData, attributes: nil)
        }
    }

    func fetchImage(imageName: String) -> Data? {
        let imagePath = (self.getDirectoryPath() as NSString).appendingPathComponent(imageName)
        if fileManager.fileExists(atPath: imagePath) {
            let fileUrl = URL(fileURLWithPath: imagePath)
            return try? Data(contentsOf: fileUrl)
        } else {
            return nil
        }
    }
}

private extension GeneralImageCacheService {
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
}

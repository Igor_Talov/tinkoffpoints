//
//  ImageCacheService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

protocol ImageCacheService {
    func save(imageData: Data, imageName: String)
    func fetchImage(imageName: String) -> Data?
}

//
//  DepositPoint.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct DepositPoint: Codable {
    let externalId: String
    let partnerName: String?
    let location: Location
    let workHours: String?
    let phones: String?
    let addressInfo: String?
    let fullAddress: String?
}

struct Location: Codable {
    let latitude: Double
    let longitude: Double
}

extension DepositPoint: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(externalId)
    }

    static func == (lhs: DepositPoint, rhs: DepositPoint) -> Bool {
        lhs.externalId == rhs.externalId
    }
}

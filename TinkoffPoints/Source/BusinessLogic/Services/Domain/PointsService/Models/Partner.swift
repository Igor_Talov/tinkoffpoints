//
//  Partner.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct Partner: Decodable {
    let id: String
    let name: String
    let picture: String
    let url: String
}

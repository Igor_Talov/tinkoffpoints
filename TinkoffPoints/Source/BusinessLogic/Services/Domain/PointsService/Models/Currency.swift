//
//  Currency.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct Currency: Decodable {
    let code: Int
    let name: String
    let strCode: String
}

//
//  Response.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct Response<T: Decodable>: Decodable {
    let resultCode: String
    let payload: T?
}

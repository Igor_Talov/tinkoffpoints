//
//  GeneralPointService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import UIKit

private struct Constants {
    static let lastModifiedDateKey = "lastModifiedDateKey"
}

class GeneralPointService {
    // MARK: - Private properties
    private let imageCacheService: ImageCacheService
    private let urlSessionController: URLSessionController
    private let keyValueStorage: KeyValueStorage
    private lazy var workQueue = OperationQueue(name: "com.tinkoffpoint.pointservice")

    private(set) lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
        return dateFormatter
    }()

    // MARK: - Initialization
    init(urlSessionController: URLSessionController,
         imageCacheService: ImageCacheService,
         keyValueStorage: KeyValueStorage) {
        self.urlSessionController = urlSessionController
        self.imageCacheService = imageCacheService
        self.keyValueStorage = keyValueStorage
    }
}

extension GeneralPointService: PointsService {
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        let dpi = UIScreen.main.dpi.rawValue
        if let lastModifiedDate = keyValueStorage.object(forKey: Constants.lastModifiedDateKey) as? Date {
            checkLastModified(imageName: imageName,
                              dpi: dpi,
                              date: lastModifiedDate) { result in
                                if result {
                                    self.downloadImage(imageName: imageName, completion: completion)
                                } else {
                                    if let imageData = self.imageCacheService.fetchImage(imageName: imageName) {
                                        let ciimage = CIImage(data: imageData)
                                        completion(ciimage)
                                    } else {
                                        completion(nil)
                                    }
                                }
            }
        } else {
            self.downloadImage(imageName: imageName, completion: completion)
        }
    }

    func fetchPartners(accountType: String,
                       success: (([Partner]) -> Void)?,
                       failure: ((Error) -> Void)?) {
        workQueue.addOperation {
            let endpoint = TinkoffEndpoint.getPartners(accountTtype: accountType)
            self.urlSessionController.perform(endpoint: endpoint) { data, _, error   in

                if let error = error {
                    failure?(error)
                    return
                }

                guard let data = data else {
                    return
                }
                do {
                    let response = try JSONDecoder().decode(Response<[Partner]>.self,
                                                            from: data)
                    guard let partners = response.payload else {
                        return
                    }
                    success?(partners)
                } catch {
                    failure?(error)
                }
            }
        }
    }

    func fetchPoints(cooddinates: CLLocationCoordinate2D,
                     radius: Int,
                     success: (([DepositPoint]) -> Void)?,
                     failure: ((Error) -> Void)?) {
        workQueue.addOperation {
            let endpoint = TinkoffEndpoint.getPoint(coordinates: cooddinates, radius: radius)
            self.urlSessionController.perform(endpoint: endpoint) { data, _, error   in
                guard let data = data else {
                    return
                }
                do {
                    let response = try JSONDecoder().decode(Response<[DepositPoint]>.self,
                                                            from: data)
                    guard let points = response.payload else {
                        return
                    }

                    success?(points)
                } catch {
                    failure?(error)
                }
            }
        }
    }
}

private extension GeneralPointService {

    func downloadImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        workQueue.addOperation {
            let dpi = UIScreen.main.dpi.rawValue
            let endpoint = TinkoffEndpoint.getImage(name: imageName, dpi: dpi)
            self.urlSessionController.perform(endpoint: endpoint) { [weak self] data, _, error  in
                guard let self = self else {
                    return
                }
                if error != nil {
                    completion(nil)
                    return
                }
                guard let data = data else {
                    return
                }
                self.imageCacheService.save(imageData: data, imageName: imageName)
                let ciimage = CIImage(data: data)
                completion(ciimage)
            }
        }
    }

    func checkLastModified(imageName: String,
                           dpi: String,
                           date: Date,
                           shouldLoadBlock: @escaping (Bool) -> Void) {
        workQueue.addOperation {
            let endpoint = TinkoffEndpoint.getImage(name: imageName, dpi: dpi)
            self.urlSessionController.perform(endpoint: endpoint) { [weak self] _, response, _  in
                guard let self = self,
                    let response: HTTPURLResponse = response as? HTTPURLResponse else {
                    shouldLoadBlock(false)
                    return
                }
                if let dateString = response.allHeaderFields["Last-Modified"] as? String {
                    if let serverDate = self.dateFormatter.date(from: dateString) {
                        shouldLoadBlock(self.compareDates(date: serverDate, otherDate: date))
                    } else {
                        shouldLoadBlock(true)
                    }
                }
            }
        }
    }

    func compareDates(date: Date, otherDate: Date) -> Bool {
        return date > otherDate
    }
}

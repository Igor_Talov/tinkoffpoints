//
//  PointsService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 09/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import CoreLocation
import Foundation

typealias ImageData = CIImage

protocol PointsService: class {
    func fetchPoints(cooddinates: CLLocationCoordinate2D,
                     radius: Int,
                     success: (([DepositPoint]) -> Void)?,
                     failure: ((Error) -> Void)?)

    func fetchPartners(accountType: String,
                       success: (([Partner]) -> Void)?,
                       failure: ((Error) -> Void)?)

    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void)
}

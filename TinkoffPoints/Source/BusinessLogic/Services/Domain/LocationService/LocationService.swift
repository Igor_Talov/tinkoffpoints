//
//  LocationService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 09/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation

protocol LocationService {
    func getCurrentLocation() -> CLLocation?
}

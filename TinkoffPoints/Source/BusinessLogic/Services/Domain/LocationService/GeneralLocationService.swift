//
//  GeneralLocationService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 09/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import Foundation

class GeneralLocationService: NSObject {
    // MARK: - Private properties
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation?

    override init() {
        super.init()
        setupLocationManager()
    }
}

private extension GeneralLocationService {
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
}

extension GeneralLocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }

        currentLocation = location
    }
}

extension GeneralLocationService: LocationService {
    func getCurrentLocation() -> CLLocation? {
        return currentLocation
    }
}

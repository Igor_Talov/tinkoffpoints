//
//  CoreDataMapperService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreData
import Foundation

protocol CoreDataMapperService: class {
    func save(points: [DepositPoint]?)

    func fetchPoints(latitude: Double, longtitude: Double) -> [DepositPoint]?

    func fetchPoints(partners: [Partner]) -> [DepositPoint]?

    func save(partners: [Partner]?)

    func fetchPartners() -> [Partner]
}

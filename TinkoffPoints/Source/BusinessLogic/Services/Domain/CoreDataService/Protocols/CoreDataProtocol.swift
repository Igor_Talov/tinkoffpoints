//
//  CoreDataProtocol.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreData
import Foundation

protocol CoreDataProtocol {

    var persistentContainer: NSPersistentContainer { get }

    var viewContext: NSManagedObjectContext { get }

    var masterContext: NSManagedObjectContext { get }

    func makeSyncContext() -> NSManagedObjectContext
}

//
//  GeneralCoreDataMapperService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreData
import Foundation

private struct Constants {
    static let depositPointEntity = "DepositPointEntity"
    static let partnerEntity = "PartnerEntity"
}

class GeneralCoreDataMapperService {
    // MARK: - Private properties
    private let coreDataService: CoreDataProtocol

    // MARK: - Initialization
    init(coreDataService: CoreDataProtocol) {
        self.coreDataService = coreDataService
    }
}

private extension GeneralCoreDataMapperService {
    func fetch(context: NSManagedObjectContext,
               entityName: String,
               predicate: NSPredicate?) -> [NSManagedObject] {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityName)
        fetchRequest.predicate = predicate

        var result: [NSManagedObject] = []
        do {
            result = try context.fetch(fetchRequest)
        } catch {
            assertionFailure(error.localizedDescription)
        }
        return result
    }

    func fetchPoints() -> [DepositPoint] {
        let masterContext = coreDataService.masterContext
        let entities = self.fetch(context: masterContext,
                                  entityName: Constants.depositPointEntity,
                                  predicate: nil)

        let models = entities.compactMap { entity -> DepositPoint? in
            guard let id = entity.value(forKey: "id") as? String,
                let latitude = entity.value(forKey: "latitude") as? Double,
                let longitude = entity.value(forKey: "longitude") as? Double else {
                    return nil
            }

            let location = Location(latitude: latitude,
                                    longitude: longitude)
            let workHourses = entity.value(forKey: "work_hours") as? String
            let address = entity.value(forKey: "address") as? String

            return DepositPoint(externalId: id,
                                partnerName: nil,
                                location: location,
                                workHours: workHourses,
                                phones: nil,
                                addressInfo: address,
                                fullAddress: address)
        }

        return models
    }
}

extension GeneralCoreDataMapperService: CoreDataMapperService {
    func fetchPoints(partners: [Partner]) -> [DepositPoint]? {
        let context = coreDataService.masterContext
        let predicate = NSPredicate(format: "partner_name IN %@", partners)
        let entities = self.fetch(context: context,
                                  entityName: Constants.depositPointEntity,
                                  predicate: predicate)

        let models = entities.compactMap { entity -> DepositPoint? in
            guard let id = entity.value(forKey: "id") as? String,
                let latitude = entity.value(forKey: "latitude") as? Double,
                let longitude = entity.value(forKey: "longitude") as? Double else {
                    return nil
            }

            let location = Location(latitude: latitude,
                                    longitude: longitude)
            let workHourses = entity.value(forKey: "work_hours") as? String
            let address = entity.value(forKey: "address") as? String

            return DepositPoint(externalId: id,
                                partnerName: nil,
                                location: location,
                                workHours: workHourses,
                                phones: nil,
                                addressInfo: address,
                                fullAddress: address)
        }

        return models
    }

    func save(partners: [Partner]?) {
        guard let partners = partners else {
            return
        }

        coreDataService.persistentContainer.performBackgroundTask { context in
            partners.forEach { partner in
                let entity = NSEntityDescription.insertNewObject(forEntityName: Constants.partnerEntity,
                                                                 into: context)

                entity.setValue(partner.id, forKeyPath: "id")
                entity.setValue(partner.name, forKeyPath: "name")
                entity.setValue(partner.picture, forKeyPath: "picture")
            }
            do {
                try context.save()
            } catch {
                assertionFailure(error.localizedDescription)
            }
        }
    }

    func fetchPartners() -> [Partner] {
        let context = coreDataService.masterContext
        let entities = self.fetch(context: context,
                                  entityName: Constants.partnerEntity,
                                  predicate: nil)

        let models = entities.compactMap { entity -> Partner? in
            guard let id = entity.value(forKey: "id") as? String else {
                    return nil
            }

            let name = entity.value(forKey: "name") as? String
            let picture = entity.value(forKey: "picture") as? String
            let url = entity.value(forKey: "url") as? String

            return Partner(id: id,
                           name: name ?? "",
                           picture: picture ?? "",
                           url: url ?? "")
        }

        return models
    }

    func save(points: [DepositPoint]?) {
        guard let points = points else {
            return
        }

        coreDataService.persistentContainer.performBackgroundTask { context in
            points.forEach { depositPoint in
                let entity = NSEntityDescription.insertNewObject(forEntityName: Constants.depositPointEntity,
                                                                 into: context)
                entity.setValue(depositPoint.externalId, forKeyPath: "id")
                entity.setValue(depositPoint.addressInfo, forKeyPath: "address")
                entity.setValue(depositPoint.location.latitude, forKeyPath: "latitude")
                entity.setValue(depositPoint.location.longitude, forKeyPath: "longitude")
                entity.setValue(depositPoint.partnerName, forKeyPath: "partner_name")
                entity.setValue(depositPoint.workHours, forKeyPath: "work_hours")
            }
            do {
                try context.save()
            } catch {
                assertionFailure(error.localizedDescription)
            }
        }
    }

    func fetchPoints(latitude: Double, longtitude: Double) -> [DepositPoint]? {
        return nil
    }
}

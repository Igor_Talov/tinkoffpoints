//
//  CoreDataService.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreData
import Foundation

private let modelName = "TinkoffPoints"

class CoreDataService: CoreDataProtocol {

    static let sharedInstance = CoreDataService()

    private init() {}

    // MARK: - CoreDataProtocol implementation
    private(set) lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: modelName)
        container.loadPersistentStores { _, error in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        return container
    }()

    private(set) lazy var viewContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)

        managedObjectContext.parent = self.masterContext

        NotificationCenter.default.addObserver(self, selector: #selector(managedObjectContextDidSave), name: NSNotification.Name.NSManagedObjectContextDidSave, object: saveContext)
        return managedObjectContext
    }()

    private(set) lazy var masterContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

        managedObjectContext.persistentStoreCoordinator = self.persistentContainer.persistentStoreCoordinator

        return managedObjectContext
    }()

    func makeSyncContext() -> NSManagedObjectContext {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.parent = self.masterContext
        return managedObjectContext
    }

    // MARK: - Private Methods
    @objc
    private func managedObjectContextDidSave(notification: NSNotification) {
        self.viewContext.mergeChanges(fromContextDidSave: notification as Notification)

        if let parentContext = self.viewContext.parent {
            saveBackgroundContext(context: parentContext, wait: true)
        }
    }

    // MARK: - Public Methods
    func saveContext() {
        let context = masterContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

private extension CoreDataService {
    func saveBackgroundContext(context: NSManagedObjectContext, wait: Bool) {
        let saveClosure = {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
        if context.hasChanges {
            if wait {
                context.performAndWait {
                    saveClosure()
                }
            } else {
                context.perform {
                    saveClosure()
                }
            }

        }
    }
}

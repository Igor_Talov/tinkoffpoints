//
//  Fonts.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol Fonts {
    var title2: UIFont { get }
    var body: UIFont { get }
    var footnote: UIFont { get }
    var caption: UIFont { get }
}

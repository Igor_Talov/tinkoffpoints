//
//  Assets.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

protocol Assets {
    var colors: Colors { get }
    var fonts: Fonts { get }
    var images: Images { get }
}

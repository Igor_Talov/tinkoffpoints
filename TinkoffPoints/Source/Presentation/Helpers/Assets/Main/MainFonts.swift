//
//  MainFonts.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

final class MainFonts: Fonts {

    private(set) lazy var title2: UIFont = {
        let fontName = "Roboto-Bold"
        return makeFont(name: fontName, size: 20.0)
    }()

    private(set) lazy var body: UIFont = {
        let fontName = "Roboto-Regular"
        return makeFont(name: fontName, size: 13.0)
    }()

    private(set) lazy var footnote: UIFont = {
        let fontName = "Roboto-bold"
        return makeFont(name: fontName, size: 12.0)
    }()

    private(set) lazy var caption: UIFont = {
        let fontName = "Roboto-Regular"
        return makeFont(name: fontName, size: 12.0)
    }()
}

private extension MainFonts {
    func makeFont(name: String, size: CGFloat) -> UIFont {
        guard let font = UIFont(name: name, size: size) else {
            assertionFailure("Failed to load the \(name) font")
            return UIFont.systemFont(ofSize: size)
        }
        return font
    }
}

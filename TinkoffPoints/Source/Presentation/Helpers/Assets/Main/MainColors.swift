//
//  MainColors.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

final class MainColors: Colors {

    private(set) lazy var primaryContent: UIColor = {
        let color = UIColor(hexRGB: 0xFFFFFF)
        return color
    }()

    private(set) lazy var primaryText: UIColor = {
        let color = UIColor(hexRGB: 0x262628)
        return color
    }()

    private(set) lazy var toolBackground: UIColor = {
        let color = UIColor(hexRGB: 0x323235)
        return color
    }()

    private(set) lazy var secondary: UIColor = {
        let color = UIColor(hexRGB: 0xe8e8ea)
        return color
    }()

    private(set) lazy var selected: UIColor = {
        let color = UIColor(hexRGB: 0xFCE76C)
        return color
    }()

    private(set) lazy var unselected: UIColor = {
        let color = UIColor(hexRGB: 0xFFFFFF, alpha: 0.3)
        return color
    }()

    private(set) lazy var defaultShadow: UIColor = {
        let color = UIColor(hexRGB: 0x000000, alpha: 0.25)
        return color
    }()
}

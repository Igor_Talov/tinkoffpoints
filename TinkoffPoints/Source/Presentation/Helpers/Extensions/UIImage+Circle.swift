//
//  UIImage+Circle.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIImage {
    func circleImage(for targetSize: CGSize) -> UIImage? {

        let side = min(targetSize.width, targetSize.height)

        UIGraphicsBeginImageContextWithOptions(targetSize, false, UIScreen.main.scale)

        defer {  UIGraphicsEndImageContext() }

        guard let context = UIGraphicsGetCurrentContext()
            else { return nil }

        self.draw(in: CGRect(origin: .zero, size: targetSize), blendMode: .copy, alpha: 1.0)

        context.setBlendMode(.copy)
        context.setFillColor(UIColor.clear.cgColor)

        let rect = CGRect(origin: .zero, size: targetSize)
        let rectPath = UIBezierPath(rect: rect)
        let circlePath = UIBezierPath(ovalIn: rect)
        rectPath.append(circlePath)
        rectPath.usesEvenOddFillRule = true
        rectPath.fill()

        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

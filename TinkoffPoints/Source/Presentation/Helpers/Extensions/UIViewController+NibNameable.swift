//
//  UIViewController+NibNameable.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIViewController: NibNameable {
    static var nibName: String {
        return String(describing: self)
    }
}

//
//  UIColor+ConversationMethods.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIColor {
    @objc
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: 1.0)
    }

    @objc
    convenience init(hexRGB: Int) {
        self.init(red: CGFloat((hexRGB >> 16) & 0xff) / 255.0,
                  green: CGFloat((hexRGB >> 8) & 0xff) / 255.0,
                  blue: CGFloat(hexRGB & 0xff) / 255.0,
                  alpha: 1.0)
    }

    @objc
    convenience init(hexRGBA: Int) {
        self.init(red: CGFloat((hexRGBA >> 24) & 0xff) / 255.0,
                  green: CGFloat((hexRGBA >> 16) & 0xff) / 255.0,
                  blue: CGFloat((hexRGBA >> 8) & 0xff) / 255.0,
                  alpha: CGFloat(hexRGBA & 0xff) / 0xff)
    }

    @objc
    convenience init(hexRGB: Int, alpha: CGFloat) {
        self.init(red: CGFloat((hexRGB >> 16) & 0xff) / 255.0,
                  green: CGFloat((hexRGB >> 8) & 0xff) / 255.0,
                  blue: CGFloat(hexRGB & 0xff) / 255.0,
                  alpha: alpha)
    }
}

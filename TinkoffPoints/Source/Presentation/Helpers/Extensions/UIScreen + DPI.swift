//
//  UIScreen + DPI.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIScreen {

    var dpi: Dpi {
        switch self.scale {
        case 2.0:
            return .xhdpi
        case 3.0:
            return .xxhdpi
        default:
            return .mdpi
        }
    }

    enum Dpi: String {
        case mdpi
        case hdpi
        case xhdpi
        case xxhdpi
    }
}

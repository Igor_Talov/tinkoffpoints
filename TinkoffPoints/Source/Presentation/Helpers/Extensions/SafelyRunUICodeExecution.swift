//
//  SafelyRunUICodeExecution.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

func safelyRunUICode(_ closure: @escaping () -> Void) {
    guard Thread.isMainThread else {
        DispatchQueue.main.async(execute: closure)
        return
    }

    closure()
}

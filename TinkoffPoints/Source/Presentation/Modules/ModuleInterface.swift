//
//  ModuleInterface.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol ModuleView: class {}

struct Module<ModuleInput> {
    let input: ModuleInput
    let view: ModuleView
}

struct ModuleInterface<ModuleInput> {
    let input: ModuleInput
    let rootViewController: UIViewController
}

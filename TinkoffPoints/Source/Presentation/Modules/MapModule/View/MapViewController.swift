//
//  MapViewController.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import MapKit
import UIKit

private struct Constants {
    static let distance: Double = 2000
    static let zoomLevel: Double = 0.5

    static let annotationIdentifier = "DeposiPointAnnotation"
}

class MapViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Public properties
    var output: MapViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var mapView: MKMapView?
    @IBOutlet private weak var zoomInButton: UIButton?
    @IBOutlet private weak var zoomOutButton: UIButton?
    @IBOutlet private weak var currentLocationButton: UIButton?

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)),
                   bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        self.mapView?.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output?.viewDidAppear()
    }
}

private extension MapViewController {
    @IBAction func zoomInButtonTapped(_ sender: UIButton) {
        output?.didTapZoomInButton()
    }

    @IBAction func zoomOutButtonTapped(_ sender: UIButton) {
        output?.didTapZoomOutButton()
    }

    @IBAction func currentLocationButtonTapped(_ sender: UIButton) {
        output?.didTapCurrentLocationButton()
    }
}

private extension MapViewController {

}

extension MapViewController: MapViewInput {
    func hideAlert() {
        safelyRunUICode {
            self.dismiss(animated: true, completion: nil)
        }
    }

    func displayError(viewModel: AlertViewModel) {
        safelyRunUICode {
            let alertController = UIAlertController(title: viewModel.title,
                                                    message: viewModel.text,
                                                    preferredStyle: .alert)
            if viewModel.okButtonTitle != nil,
                viewModel.okAction != nil {
                let okAlertAction = UIAlertAction(title: viewModel.okButtonTitle,
                                                  style: .default) { _ in
                                                    viewModel.okAction?()
                }
                alertController.addAction(okAlertAction)
            }

            if viewModel.cancelButtonTitle != nil,
                viewModel.cancelAction != nil {
                let cancelAlertAction = UIAlertAction(title: viewModel.cancelButtonTitle,
                                                      style: .cancel) { _ in
                                                        viewModel.cancelAction?()
                }
                alertController.addAction(cancelAlertAction)
            }

            self.present(alertController,
                         animated: true,
                         completion: nil)
        }
    }

    func displayAnnotations(viewModels: [PointAnnotationViewModel]) {
        safelyRunUICode {
            self.mapView?.addAnnotations(viewModels)
        }
    }

    func zoomInMapView() {
        safelyRunUICode {
            if let currentRegion = self.mapView?.region {
                let span = MKCoordinateSpan(latitudeDelta: currentRegion.span.latitudeDelta * Constants.zoomLevel,
                                            longitudeDelta: currentRegion.span.longitudeDelta * Constants.zoomLevel)
                let newRegion = MKCoordinateRegion(center: currentRegion.center, span: span)
                self.mapView?.setRegion(newRegion, animated: true)
            }
        }
    }

    func zoomOutMapView() {
        safelyRunUICode {
            if let currentRegion = self.mapView?.region {
                let span = MKCoordinateSpan(latitudeDelta: currentRegion.span.latitudeDelta / Constants.zoomLevel,
                                            longitudeDelta: currentRegion.span.longitudeDelta / Constants.zoomLevel)
                let newRegion = MKCoordinateRegion(center: currentRegion.center, span: span)
                self.mapView?.setRegion(newRegion, animated: true)
            }
        }
    }

    func displayRegion(location: CLLocation) {
        safelyRunUICode {
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                                longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center,
                                            latitudinalMeters: Constants.distance,
                                            longitudinalMeters: Constants.distance)
            self.mapView?.setRegion(region, animated: true)
        }
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView,
                 regionDidChangeAnimated animated: Bool) {
        let radius = mapView.currentRadius()
        if radius < 50000.0 {
            output?.mapDidChangeVisibleRect(rect: mapView.visibleMapRect, radius: radius)
        }

    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? PointAnnotationViewModel else {
            return nil
        }

        let view: PointAnnotationView
        if let dequeuedView = self.mapView?.dequeueReusableAnnotationView(withIdentifier: Constants.annotationIdentifier) as? PointAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = PointAnnotationView(annotation: annotation,
                                       reuseIdentifier: Constants.annotationIdentifier)
        }
        view.frame.size = CGSize(width: view.intrinsicContentSize.width,
                                 height: view.intrinsicContentSize.height)
        view.backgroundColor = style.assets.colors.selected

        let loadImageClosure: (CIImage?) -> Void = { ciimage in
            guard let ciimage = ciimage else {
                return
            }
            let image = UIImage(ciImage: ciimage)
            safelyRunUICode {
                view.backgroundColor = .clear
                view.image = image
            }
        }

        if let imageName = annotation.picture {
             output?.fetchImage(imageName: imageName, completion: loadImageClosure)
        }

        return view
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? PointAnnotationViewModel else {
            return
        }
        annotation.tapAction?()
    }
}

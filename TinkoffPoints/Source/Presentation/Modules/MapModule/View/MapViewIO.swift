//
//  MapViewIO.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import MapKit

// MARK: - MapViewInput
protocol MapViewInput: class {
    func displayRegion(location: CLLocation)
    func displayError(viewModel: AlertViewModel)
    func displayAnnotations(viewModels: [PointAnnotationViewModel])
    func zoomInMapView()
    func zoomOutMapView()

    func hideAlert()
}

// MARK: - MapViewutput
protocol MapViewOutput: class {
    // MARK: - Life cycle
    func viewDidLoad()
    func viewDidAppear()
    func mapDidChangeVisibleRect(rect: MKMapRect, radius: Double)

    // MARK: - Actions
    func didTapCurrentLocationButton()
    func didTapZoomInButton()
    func didTapZoomOutButton()

    // MARK: - Fetching
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void)
}

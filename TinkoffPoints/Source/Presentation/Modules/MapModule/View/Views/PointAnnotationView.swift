//
//  PointAnnotationView.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 11/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import MapKit

private struct Constants {
    static let size = CGSize(width: 30, height: 30)
}

class PointAnnotationView: MKAnnotationView {
    // MARK: - Private properties

    private lazy var imageView: UIImageView = {
        let rect = CGRect(origin: .zero,
                          size: Constants.size)
        let imageView = UIImageView(frame: rect)
        imageView.contentMode = .center
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        setupViews()
    }

    override var image: UIImage? {
        didSet {
            safelyRunUICode { [weak self] in
                guard let self = self else { return }
                self.imageView.image = self.image?.circleImage(for: Constants.size)
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var intrinsicContentSize: CGSize {
        return Constants.size
    }
}

private extension PointAnnotationView {
    func setupViews() {
        addSubview(imageView)

        NSLayoutConstraint.activate([
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.rightAnchor.constraint(equalTo: rightAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])

        layer.frame = CGRect(origin: .zero, size: Constants.size)
        layer.cornerRadius = bounds.size.width / 2
    }
}

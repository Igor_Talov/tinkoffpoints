//
//  GeneralMapCoordinator.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralMapCoordinator: MapCoordinator {
    // MARK: - Private properties
    private let style: Style
    private(set) var isStarted = false
    private let moduleFactory: MapModuleFactory

    private var childCoordinators: [Coordinator] = []

    private var transitionController: PanelTransition?

    var rootNavigationController: UINavigationController?

    init(style: Style,
         moduleFactory: MapModuleFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
    }

    // MARK: - MapCoordinator
    func start(on rootController: UINavigationController) {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return
        }
        isStarted = true
        rootNavigationController = rootController

        let module = moduleFactory.makeMapModule(output: self)
        let controller = module.rootViewController
        rootController.setViewControllers([controller], animated: false)
    }
}

extension GeneralMapCoordinator: PointDetailModuleOutput { }

extension GeneralMapCoordinator: MapModuleOutput {
    func mapModule(_ module: MapModuleInput, didRequest pointDetail: DepositPoint) {
        let detailModule = moduleFactory.makeDetailsModule(output: self)
        detailModule.input.start(with: pointDetail)
        let controller = detailModule.rootViewController

        transitionController = PanelTransition(style: style)
        transitionController?.dismissalTransitionDidEnd = { [weak self] in
            self?.transitionController = nil
        }

        controller.modalPresentationStyle = .custom
        controller.transitioningDelegate = transitionController
        rootNavigationController?.topViewController?.present(controller, animated: true)
    }
}

//
//  MapCoordinator.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol MapCoordinator: Coordinator {

    var isStarted: Bool { get }
    var rootNavigationController: UINavigationController? { get }

    func start(on rootController: UINavigationController)
}

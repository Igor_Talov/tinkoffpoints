//
//  MapModuleBuilder.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class MapModuleBuilder {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }

    func buildModule(with output: MapModuleOutput) -> ModuleInterface<MapModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)
        presenter.output = output
        interactor.output = presenter
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension MapModuleBuilder {
    func makeView() -> MapViewController {
        let controller = MapViewController(style: style)
        return controller
    }

    func makeInteractor() -> MapInteractor {
        let locationService = serviceProvider.locationService
        let urlSessionController = serviceProvider.urlSessionController
        let imageCacheService = serviceProvider.imageCacheService
        let coreDataMapperService = serviceProvider.coreDataMapperService
        let keyValueStorage = serviceProvider.keyValueStorage
        let pointService = GeneralPointService(urlSessionController: urlSessionController,
                                               imageCacheService: imageCacheService,
                                               keyValueStorage: keyValueStorage)
        let interactor = MapInteractor(locationService: locationService,
                                       pointsService: pointService,
                                       coreDataMapperService: coreDataMapperService)
        return interactor
    }
    func makePresenter(view: MapViewInput,
                       interactor: MapInteractorInput) -> MapPresenter {
        let presenter = MapPresenter(view: view,
                                     interactor: interactor)
        return presenter
    }
}

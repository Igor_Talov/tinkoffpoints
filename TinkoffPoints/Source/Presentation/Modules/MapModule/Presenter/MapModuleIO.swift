//
//  MapModuleIO.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - MapModuleInput
protocol MapModuleInput: class {
}

// MARK: - MapModuleOutput
protocol MapModuleOutput: class {
    func mapModule(_ module: MapModuleInput, didRequest pointDetail: DepositPoint)
}

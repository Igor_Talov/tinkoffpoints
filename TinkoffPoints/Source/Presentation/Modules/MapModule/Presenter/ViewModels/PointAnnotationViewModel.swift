//
//  PointAnnotationViewModel.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import MapKit

class PointAnnotationViewModel: NSObject, MKAnnotation {

    var id: String
    var picture: String?
    var coordinate: CLLocationCoordinate2D
    var tapAction: (() -> Void)?

    init(id: String,
         picture: String?,
         coordinate: CLLocationCoordinate2D,
         tapAction: (() -> Void)?) {
        self.id = id
        self.picture = picture
        self.coordinate = coordinate
        self.tapAction = tapAction
        super.init()
    }
}

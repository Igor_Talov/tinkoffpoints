//
//  MapPresenter.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import Foundation
import MapKit

typealias Closure = (CIImage?) -> Void

class MapPresenter {
    // MARK: - Private properties
    private weak var view: MapViewInput?
    private let interactor: MapInteractorInput

    // MARK: - Public properties
    weak var output: MapModuleOutput?

    // MARK: - Initialization
    init(view: MapViewInput,
         interactor: MapInteractorInput) {
        self.view = view
        self.interactor = interactor
    }
}

private extension MapPresenter {
    func fetchImage(imageName: String?, completion: @escaping (ImageData?) -> Void) {
        guard let imageName = imageName else {
            completion(nil)
            return
        }
        interactor.fetchImage(imageName: imageName, completion: completion)
    }

    func didRequestDepositPointDetails(depositPoint: DepositPoint) {
        output?.mapModule(self, didRequest: depositPoint)
    }

    func displayAlert(error: Error) {
        let cancelAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.view?.hideAlert()
        }

        let alertViewModel = makeAlertViewModel(error,
                                                okAction: nil,
                                                cancelAction: cancelAction)
        view?.displayError(viewModel: alertViewModel)
    }

    func makeAlertViewModel(_ error: Error, okAction: (() -> Void)?, cancelAction: (() -> Void)?) -> AlertViewModel {
        let alertText = error.localizedDescription
        let errorTitle = NSLocalizedString("ERROR", comment: "Error alert title")
        let viewModel = AlertViewModel(title: errorTitle,
                                       text: alertText,
                                       okButtonTitle: NSLocalizedString("RETRY", comment: ""),
                                       cancelButtonTitle: NSLocalizedString("CANCEL", comment: ""),
                                       okAction: okAction,
                                       cancelAction: cancelAction)
        return viewModel
    }
}

extension MapPresenter: MapModuleInput {
}

extension MapPresenter: MapInteractorOutput {
    func didFetchPoints(with error: Error) {
        // create error viewModel and display error
    }

    func didFetchPoints(points: [DepositPoint]) {
        let viewModels = points.compactMap { point -> PointAnnotationViewModel? in
            let tapAction: () -> Void = { [weak self] in
                guard let self = self else {
                    return
                }
                self.didRequestDepositPointDetails(depositPoint: point)
            }

            let coordinate = CLLocationCoordinate2D(latitude: point.location.latitude,
                                                    longitude: point.location.longitude)
            return PointAnnotationViewModel(id: point.externalId,
                                            picture: point.partnerName,
                                            coordinate: coordinate,
                                            tapAction: tapAction)
        }

        view?.displayAnnotations(viewModels: viewModels)
    }

    func didFetchCurrentLocation(location: CLLocation) {
        view?.displayRegion(location: location)
    }
}

extension MapPresenter: MapViewOutput {
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        interactor.fetchImage(imageName: imageName, completion: completion)
    }

    func viewDidAppear() {
        interactor.fetchCurrentLocation()
    }

    func mapDidChangeVisibleRect(rect: MKMapRect, radius: Double) {
        interactor.fetchPoints(for: rect, radius: radius)
    }

    func didTapCurrentLocationButton() {
        interactor.fetchCurrentLocation()
    }

    func didTapZoomInButton() {
        view?.zoomInMapView()
    }

    func didTapZoomOutButton() {
        view?.zoomOutMapView()
    }

    func viewDidLoad() {
        interactor.fetchPartners { [weak self] error in
            guard let self = self else {
                return
            }

            if let error = error {
                self.displayAlert(error: error)
            }
        }
    }
}

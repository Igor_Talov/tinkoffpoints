//
//  MapInteractorIO.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreLocation
import MapKit

// MARK: - MapInteractorInput
protocol MapInteractorInput: class {
    func fetchCurrentLocation()
    func fetchPartners(completion: @escaping (Error?) -> Void)
    func fetchPoints(for mapRect: MKMapRect, radius: Double)
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void)
}

// MARK: - MapInteractorOutput
protocol MapInteractorOutput: class {
    func didFetchCurrentLocation(location: CLLocation)
    func didFetchPoints(points: [DepositPoint])
    func didFetchPoints(with error: Error)
}

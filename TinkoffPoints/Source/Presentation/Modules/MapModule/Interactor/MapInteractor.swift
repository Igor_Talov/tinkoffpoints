//
//  MapInteractor.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import MapKit

private struct Constants {
    static let accountType = "Credit"
}

class MapInteractor {
    // MARK: - Private properties
    private let locationService: LocationService
    private let pointsService: PointsService
    private let coreDataMapperService: CoreDataMapperService

    // MARK: - Public properties
    var output: MapInteractorOutput?

    // MARK: - Initialization
    init(locationService: LocationService,
         pointsService: PointsService,
         coreDataMapperService: CoreDataMapperService) {
        self.locationService = locationService
        self.pointsService = pointsService
        self.coreDataMapperService = coreDataMapperService
    }
}

extension MapInteractor: MapInteractorInput {
    func fetchPartners(completion: @escaping (Error?) -> Void) {
        pointsService
            .fetchPartners(accountType: Constants.accountType,
                           success: { [weak self] partners in
                            guard let self = self else {
                                return
                            }

                            if !partners.isEmpty {
                                self.savePartners(partners: partners)
                                completion(nil)
                            }
                }, failure: { error in
            completion(error)
        })
    }

    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        pointsService.fetchImage(imageName: imageName, completion: completion)
    }

    func fetchPoints(for mapRect: MKMapRect, radius: Double) {
        let coordinates = mapRect.origin.coordinate
        pointsService
            .fetchPoints(cooddinates: coordinates,
                         radius: Int(radius),
                         success: { [weak self] points in
                            guard let self = self else {
                                return
                            }

                            self.output?.didFetchPoints(points: points)
                }, failure: { [weak self] error in
                    guard let self = self else {
                        return
                    }

                    self.output?.didFetchPoints(with: error)
            })
    }

    func fetchCurrentLocation() {
        guard let location = locationService.getCurrentLocation() else {
            return
        }
        output?.didFetchCurrentLocation(location: location)
    }
}

private extension MapInteractor {
    func savePartners(partners: [Partner]) {
        self.coreDataMapperService.save(partners: partners)
    }
}

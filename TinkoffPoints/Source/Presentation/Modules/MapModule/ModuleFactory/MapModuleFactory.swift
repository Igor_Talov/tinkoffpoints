//
//  MapModuleFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol MapModuleFactory {
    func makeMapModule(output: MapModuleOutput) -> ModuleInterface<MapModuleInput>
    func makeDetailsModule(output: PointDetailModuleOutput) -> ModuleInterface<PointDetailModuleInput>
}

//
//  GeneralMapModuleFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralMapModuleFactory {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }
}

extension GeneralMapModuleFactory: MapModuleFactory {
    func makeDetailsModule(output: PointDetailModuleOutput) -> ModuleInterface<PointDetailModuleInput> {
        let moduleBuilder = PointDetailModuleBuilder(style: style,
                                                     serviceProvider: serviceProvider)
        return moduleBuilder.buildModule(with: output)
    }

    func makeMapModule(output: MapModuleOutput) -> ModuleInterface<MapModuleInput> {
        let moduleBuilder = MapModuleBuilder(style: style,
                                             serviceProvider: serviceProvider)
        return moduleBuilder.buildModule(with: output)
    }
}

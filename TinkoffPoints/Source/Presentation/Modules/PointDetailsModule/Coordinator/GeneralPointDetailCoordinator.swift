//
//  GeneralPointDetailCoordinator.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralPointDetailCoordinator: PointDetailCoordinator {
    // MARK: - Private properties
    private let style: Style
    private(set) var isStarted = false
    private let moduleFactory: PointDetailModuleFactory

    private var childCoordinators: [Coordinator] = []

    var rootController: UIViewController?

    // MARK: - Initialization
    init(style: Style,
         moduleFactory: PointDetailModuleFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
    }

    // MARK: - PointDetailCoordinator
    func start() -> UIViewController {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return UIViewController()
        }
        isStarted = true
        let module = moduleFactory.makePointDetailModuleFactory(output: self)

        let rootController = module.rootViewController
        self.rootController = rootController

        return rootController
    }
}

extension GeneralPointDetailCoordinator: PointDetailModuleOutput {
}

//
//  DetailsViewModel.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 12/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct DetailsViewModel {
    let title: String
    let subtitle: String?
    let address: String?
    let imageName: String?
}

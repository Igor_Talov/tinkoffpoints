//
//  PointDetailPresenter.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

private struct State {
    var point: DepositPoint?
}

class PointDetailPresenter {
    // MARK: - Private properties
    private weak var view: PointDetailsViewInput?
    private let interactor: PointDetailInteractorInput

    private var state = State()

    weak var output: PointDetailModuleOutput?

    // MARK: - Initialization
    init(view: PointDetailsViewInput?,
         interactor: PointDetailInteractorInput) {
        self.view = view
        self.interactor = interactor
    }
}

extension PointDetailPresenter: PointDetailModuleInput {
    func start(with point: DepositPoint) {
        state.point = point
    }
}

extension PointDetailPresenter: PointDetailInteractorOutput {
}

extension PointDetailPresenter: PointDetailsViewOutput {
    func viewDidLoad() {
        guard let point = state.point else {
            assertionFailure("point is nil")
            return
        }

        let viewModel = DetailsViewModel(title: point.partnerName ?? "",
                                         subtitle: point.workHours,
                                         address: point.fullAddress,
                                         imageName: point.partnerName)

        view?.displayDetails(viewModel: viewModel)
    }

    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        interactor.fetchImage(imageName: imageName, completion: completion)
    }
}

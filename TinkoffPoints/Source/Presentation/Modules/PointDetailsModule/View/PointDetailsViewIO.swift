//
//  PointDetailsViewIO.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol PointDetailsViewInput: class {
    func displayDetails(viewModel: DetailsViewModel)
}

protocol PointDetailsViewOutput: class {
    func viewDidLoad()

    // MARK: - Fetching
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void)
}

//
//  PointDetailViewController.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class PointDetailViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Public properties
    var output: PointDetailsViewOutput?

    // MARK: - IBOUtlets
    @IBOutlet private weak var imageView: UIImageView? {
        didSet {

        }
    }
    @IBOutlet private weak var titleLabel: UILabel? {
        didSet {
            updateTitleLabelStyle()
        }
    }
    @IBOutlet private weak var subtitleLabel: UILabel? {
        didSet {
            updateSubtitlesLabels()
        }
    }
    @IBOutlet private weak var addressLabel: UILabel? {
        didSet {
            updateSubtitlesLabels()
        }
    }

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)),
                   bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }
}

private extension PointDetailViewController {
    func updateTitleLabelStyle() {
        titleLabel?.font = style.assets.fonts.title2
    }

    func updateSubtitlesLabels() {
        subtitleLabel?.font = style.assets.fonts.body
        addressLabel?.font = style.assets.fonts.body
    }

    func updateImageViewStyle() {
        guard let imageView = imageView else {
            return
        }

        imageView.layer.cornerRadius = imageView.frame.width / 2
        imageView.layer.masksToBounds = true
    }
}

extension PointDetailViewController: PointDetailsViewInput {
    func displayDetails(viewModel: DetailsViewModel) {
        safelyRunUICode {
            self.titleLabel?.text = viewModel.title
            self.subtitleLabel?.text = viewModel.subtitle
            self.addressLabel?.text = viewModel.address

            let loadImageClosure: (CIImage?) -> Void = { ciimage in
                guard let ciimage = ciimage else {
                    return
                }
                let image = UIImage(ciImage: ciimage)
                safelyRunUICode {
                    self.imageView?.image = image
                }
            }

            if let imageName = viewModel.imageName {
                self.output?.fetchImage(imageName: imageName,
                                        completion: loadImageClosure)
            }
        }
    }
}

//
//  PointDetailInteractor.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class PointDetailInteractor {
    // MARK: - Private properties
    private let pointsService: PointsService

    // MARK: - Public properties
    weak var output: PointDetailInteractorOutput?

    // MARK: - Initialization
    init(pointsService: PointsService) {
        self.pointsService = pointsService
    }
}

extension PointDetailInteractor: PointDetailInteractorInput {
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void) {
        pointsService.fetchImage(imageName: imageName,
                                 completion: completion)
    }
}

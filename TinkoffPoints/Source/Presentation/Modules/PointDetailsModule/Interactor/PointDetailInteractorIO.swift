//
//  PointDetailInteractorIO.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol PointDetailInteractorInput: class {
    func fetchImage(imageName: String, completion: @escaping (ImageData?) -> Void)
}

protocol PointDetailInteractorOutput: class {}

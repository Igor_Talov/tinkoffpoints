//
//  PointDetailModuleBuilder.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class PointDetailModuleBuilder {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }

    func buildModule(with output: PointDetailModuleOutput) -> ModuleInterface<PointDetailModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)
        presenter.output = output
        interactor.output = presenter
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension PointDetailModuleBuilder {
    func makeView() -> PointDetailViewController {
        let controller = PointDetailViewController(style: style)
        return controller
    }

    func makeInteractor() -> PointDetailInteractor {
        let urlSessionController = serviceProvider.urlSessionController
        let imageCacheService = serviceProvider.imageCacheService
        let keyValueStorage = serviceProvider.keyValueStorage
        let pointService = GeneralPointService(urlSessionController: urlSessionController,
                                               imageCacheService: imageCacheService,
                                               keyValueStorage: keyValueStorage)
        let interactor = PointDetailInteractor(pointsService: pointService)
        return interactor
    }
    func makePresenter(view: PointDetailsViewInput,
                       interactor: PointDetailInteractorInput) -> PointDetailPresenter {
        let presenter = PointDetailPresenter(view: view,
                                             interactor: interactor)
        return presenter
    }
}

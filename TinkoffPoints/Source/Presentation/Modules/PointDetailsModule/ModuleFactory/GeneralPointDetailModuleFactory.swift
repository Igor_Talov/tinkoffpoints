//
//  GeneralPointDetailModuleFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralPointDetailModuleFactory {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }
}

extension GeneralPointDetailModuleFactory: PointDetailModuleFactory {
    func makePointDetailModuleFactory(output: PointDetailModuleOutput) -> ModuleInterface<PointDetailModuleInput> {
        let builder = PointDetailModuleBuilder(style: style,
                                               serviceProvider: serviceProvider)
        return builder.buildModule(with: output)
    }
}

//
//  PointDetailModuleFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 10/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol PointDetailModuleFactory {
    func makePointDetailModuleFactory(output: PointDetailModuleOutput) ->
        ModuleInterface<PointDetailModuleInput>
}

//
//  KeyValueStorage.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 12/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

protocol KeyValueStorage {
    func object(forKey key: String) -> Any?
    func set(_ value: Any?, forKey key: String)
}

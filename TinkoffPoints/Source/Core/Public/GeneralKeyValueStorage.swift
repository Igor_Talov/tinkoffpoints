//
//  GeneralKeyValueStorage.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 12/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralKeyValueStorage {
    // MARK: - Private properties
    private let userDefaults: UserDefaults

    // MARK: - Initialization
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
}

// MARK: - KeyValueStorage
extension GeneralKeyValueStorage: KeyValueStorage {
    func object(forKey key: String) -> Any? {
        return userDefaults.object(forKey: key)
    }

    func set(_ value: Any?, forKey key: String) {
        userDefaults.set(value, forKey: key)
    }
}

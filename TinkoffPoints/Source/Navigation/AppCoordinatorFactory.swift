//
//  AppCoordinatorFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol AppCoordinatorFactory {
    func makeMapCoordinator() -> MapCoordinator
}

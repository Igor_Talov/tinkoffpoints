//
//  GeneralAppCoordinatorFactory.swift
//  TinkoffPoints
//
//  Created by Игорь Талов on 08/11/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralAppCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style
    private let serviceProvider: ServiceProvider

    // MARK: - Initialization
    init(style: Style,
         serviceProvider: ServiceProvider) {
        self.style = style
        self.serviceProvider = serviceProvider
    }
}

extension GeneralAppCoordinatorFactory: AppCoordinatorFactory {
    func makeMapCoordinator() -> MapCoordinator {
        let modulefactory = GeneralMapModuleFactory(style: style,
                                                    serviceProvider: serviceProvider)
        let coordinator = GeneralMapCoordinator(style: style,
                                                moduleFactory: modulefactory)
        return coordinator
    }
}
